#!/usr/bin/env bash

pip3 install setuptools wheel twine --upgrade
python3 setup.py sdist bdist_wheel